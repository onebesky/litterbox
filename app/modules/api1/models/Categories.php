<?php

/**
 * 
 * Category model class will accept a JSON encoded array of data to log this information 
 * into the FreshRealm cloud. 
 *   
 * @copyright 2014 FreshRealm
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 * @author Humza Iqbal <humza@freshrealm.co>
 * 
 * @property Order $model connected active record  
 *
 * @property int published
 * @property string imageUrl
 *
 * URL is api2.freshrealm.local/categories?key=<token>
 *
 * return example:
 *
 * (
 *   'code' => 200
 *   'timestamp' => 1401215276
 *   'data' => array
 *   (
 *       'id' => '261'
 *       'name' => 'Awesomeness'
 *       'parentId' => null
 *       'published' => 'yes'
 *       'all_roles' => null
 *        'phone_number' => null
 *   )
 * )
 * 
 */

class Categories extends FrApiResource {
    /**
     * @property int $id;
     */
    public $id;

    /**
     * ParentID for the category
     * @property string $parentId;
     * 
     */
    public $parentId;

    /**
     * Name of the category to be displayed
     * @property string $name;
     */
    public $name;

    /**
     * Category is active or inactive
     * @property string $_published = 1; 
     */
    
    protected $_published = 1;

    public function activeRecordClassName(){
        return 'Category';
    }
    
    public function rules() {
        $rules = parent::rules();
        return CMap::mergeArray($rules, array(
                    array('id, name, parentId, published', 'safe', 'on' => 'view, list'),
            array('name,parentId,published', 'safe', 'on' => 'create, update'),
        ));
    }
    
    /**
     * Default criteria are always being applied to search conditions
     */
    public function defaultScope() {
        $merchant = $this->module->getAuthenticatedModel();
        // filter by merchant
        return new CDbCriteria(array(
            'condition' => 'merchant_id=:merchantId',
            'params' => array(':merchantId' => $merchant->id)
        ));
    }

    /**
     * Maps all the attributes for the category 
     * @return array 
     */

    public function attributeMap() {
        return array(
            'id' => 'id',
            'name' => 'category_name',
            'parentId' => 'parent_id',
            'published' => 'published',
        );
    }
    
    // TODO: how to load data into model variable, but display it as something else
    public function getPublished() {
        if ($this->scenario == 'create' || $this->scenario == 'update'){
            return $this->_published;
        } else {
            return $this->_published ? 'yes' : 'no';
        }
    }
    
    /**
     * If value is represented / formatted in different format, we have to create a setter / getter for the variable.
     * Naming convention is to name the variable with underscore
     * @param type $value
     */
    public function setPublished($value) {
        d("Published setter to $value");
        $this->_published = $value == 1 || $value == 'yes' ? 1 : 0;
    }
    
    public function getImageUrl() {
        
    }

    /**
     * Set merchant id value before the model is being saved
     * @return boolean
     */
    public function beforeSave(){
        if ($this->model->merchant_id == null){
            $merchant = $this->module->getAuthenticatedModel();
            $this->model->merchant_id = $merchant->id;
        }

        return true;
    }
}
