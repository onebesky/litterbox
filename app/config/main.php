<?php

/**
 *
 * main.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'main');

use Yiinitializr\Helpers\ArrayX;

// web application configuration
return ArrayX::merge(array(
            'name' => '{APPLICATION NAME}',
            // path aliases
            'aliases' => array(
                'bootstrap' => dirname(__FILE__) . '/../lib/vendor/2amigos/yiistrap',
                'yiiwheels' => dirname(__FILE__) . '/../lib/vendor/2amigos/yiiwheels',
            ),
            // application behaviors
            'behaviors' => array(),
            // controllers mappings
            'controllerMap' => array(),
            // application modules
            'modules' => array(
                'api1' => array(
                    'class' => 'application.modules.api1.ApiModule',
                    // optional configuration:        
                    //'baseUrl' => 'localhost/litterbox/api1', // skip to use path format myproject.com/api1
                    'lastUpdateAttribute' => 'update_time', // DATETIME field that contains last update time of active record
                    'format' => 'json', // only json is supported so far 
                    'authModelClass' => 'FrAuthModel', // override this class to change authentication behavior
                    'myAuthenticatedModelClass' => 'Cat', // active record that used for login
                    'myAuthenticatedModelPasswordField' => 'api_secret',
                ),
            ),
            // application components
            'components' => array(
                'bootstrap' => array(
                    'class' => 'bootstrap.components.TbApi',
                ),
                'clientScript' => array(
                    'scriptMap' => array(
                        'bootstrap.min.css' => false,
                        'bootstrap.min.js' => false,
                        'bootstrap-yii.css' => false
                    )
                ),
                'urlManager' => array(
                    // uncomment the following if you have enabled Apache's Rewrite module.
                    'urlFormat' => 'path',
                    'showScriptName' => false,
                    'rules' => array(
                        // custom actions in resource controller
                        array('api1/<controller>/<action>', 'pattern' => 'api1/<controller:\w+>/<action:\w+>/<id:\d+>'),
                        // crud for resource controller
                        array('api1/<controller>/<action>', 'pattern' => 'api1/<controller:\w+>/<action:\w+>'),
                        // everything else goes to the default controller
                        array('api1/default/<action>', 'pattern' => 'api1/<action:\w+>'),
                        
                        // default rules
                        '<controller:\w+>/<id:\d+>' => '<controller>/view',
                        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                    ),
                ),
                'user' => array(
                    // enable cookie-based authentication
                    'allowAutoLogin' => true,
                    'loginUrl' => array('site/login'),
                    'class' => 'application.extensions.components.WebUser',
                ),
            //'errorHandler' => array(
            //    'errorAction' => 'site/error',
            //)
            ),
            // application parameters
            'params' => array(),
                ), require_once('common.php'));
