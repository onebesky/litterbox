<?php

/**
 * Data used in this controller are loaded through REST api
 *
 * @author Ondrej Nebesky
 */
class RestController extends EController{
    
    /*public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('addActivity'),
                'users'=>array('?'),
            ),
        );
    }*/
    
    /**
     * Locations are cat independent - use system auth token to access
     */
    public function actionLocations(){
        
        $sdk = Yii::app()->freshSdk->getClientSdk();
        $data = $sdk->customAction('locations');
        print_r($data);
    }
    
    /**
     * Cat specific action - use user login instead
     */
    public function actionAddActivity(){
        $cat = Yii::app()->user->cat;
        $sdk = Yii::app()->freshSdk->getCatSdk($cat);
        $data = $sdk->customAction('locations');
    }
}
