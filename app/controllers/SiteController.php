<?php

/**
 *
 * SiteController class
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
class SiteController extends EController {

    public function actionIndex() {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin($id = null) {
        $model = new LoginForm;

        $catsDataProvider = new CActiveDataProvider('Cat');

        if ($id) {
            $cat = Cat::model()->findByPk($id);

            $identity = new UserIdentity($cat->id, '');
            Yii::app()->user->login($identity, 0);
            $this->redirect(Yii::app()->request->baseUrl . '/');
        }

        // display the login form
        $this->render('login', array('model' => $model, 'dataProvider' => $catsDataProvider));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
