<div class="container">
    <?php
    /* @var $this SiteController */
    /* @var $model LoginForm */
    /* @var $form CActiveForm  */

    $this->pageTitle = Yii::app()->name . ' - Login';
    $this->breadcrumbs = array(
        'Login',
    );
    ?>

    <h1>Login as Cat</h1>

    <p>Note: cats are not really good with passwords, so this page does not ask for any.</p>


    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'order-grid',
        'dataProvider' => $dataProvider,
        'columns' => array(
            'id',
            'name',
            array(
                'header' => 'Login',
                'type' => 'raw',
                'value' => function($data) {

            return TbHtml::link("Login", '/site/login/' . $data->id, ['class' => 'btn btn-small']);
        }
            )
        ),
    ));
    ?>
</div>
