<?php

/**
 * User object accessible through Yii::app()->user or user()
 */
class WebUser extends CWebUser {

    public $_cat;

    /**
     * All possible roles user can have. These names match database tables.
     * @var type 
     */
    public static $allRoles = array(
        'user',
        'admin',
       
    );


    /**
     * In other words, do we have anonymous user?
     */
    public function isGuest() {
        return $this->isGuest;
    }

    /**
     * Destroy all the records stored in the session. This is mainly useful
     * for testing, where we need to switch a couple of users inside one session.
     * @param type $destroySession
     */
    public function logout($destroySession = true) {
        parent::logout($destroySession);
        $this->_cat = null;
    }

    public function getCat(){
        if ($this->isGuest)
            return false;
        if ($this->_cat === null) {
            $this->_cat = Cat::model()->findByPk($this->id);
        }
        return $this->_cat;
    }
  
    /**
     * Redirect to login screen, when user is not logged in.
     */
    public function loginRequired() {
        if ($this->isGuest()) {
            parent::loginRequired();
        } else if ($this->isAdmin()) {
            app()->controller->redirect(url('/'));
        }

        // permission denied
        throw new CHttpException(403, 'You don\' have permission to access this page.');
    }

}
