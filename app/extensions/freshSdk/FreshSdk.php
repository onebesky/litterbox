<?php

/**
 * SDK connects to Fresh Rest API.
 *
 * Setup
 * 1) Contact FreshRealm tech support to get your password ($secret)
 * 2) Place SDK class in writable folder or set $dataPath to writable folder.
 *
 * Example
 * $sdk = new FreshRealmSdk();
 * $sdk->getServiceAvailability(93001);
 *
 * @author Ian McManus <ian@freshrealm.co>
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 * @copyright Copyright &copy; FreshRealm 2013
 * @package freshrealm.sdk
 * @version 1.0.3
 */
class FreshSdk {

    /**
     * Client authentication token obtained from FreshRealm.
     * @var string
     */
    public $password;

    /**
     * FreshRealm REST API url with version string
     * @var string
     */
    public static $url = 'http://localhost/litterbox/api1/';

    /**
     * Temporary storage for the key file. Key file contains authentication token
     * for the FreshRealm API. Make sure this location is writable.
     * @var string
     */
    public $dataPath = 'fresh.txt';

    /**
     * IP address based authentication token
     * @var string
     */
    private $token;

    /**
     * The last request sent to the Rest Api
     * @var FreshRealmRequest
     */
    public $lastRequest;
    protected $data;

    public function __construct() {
        $this->includeFiles();
    }

    /**
     * Get the last call input, output and url.
     * @param $options array See FreshRealmRequest::$defaultApiOptions
     * @return FreshRealmRequest
     */
    public function getFreshRequest($options = null) {
        $request = new FreshRequest($this);
        if ($options != null) {
            $request->setApiOptions($options);
        }
        return $request;
    }

    /**
     * Include supporting files and libraries from sdk folder.
     */
    private function includeFiles() {
        $files = array(
            'sdk/FreshRequest.php',
            'sdk/FreshResource.php',
            //'sdk/models/FreshRealmAddress.php',
            //'sdk/models/FreshRealmCartItem.php',
            //'sdk/models/FreshRealmCustomer.php',
	//		'sdk/models/FreshRealmProduct.php',
          //  'sdk/models/FreshRealmOrder.php'
        );

        $dirname = dirname(__FILE__) . '/';

        foreach ($files as $file) {
            include_once($dirname . $file);
        }
    }

    /**
     * Send secret authentication token to the API in order to get authentication token.
     * This function is called when the key file has the wrong key or a key that is no longer valid
     * @param string $password
     * @return string token used with every API call
     */
    public function authenticate($password=null) {
        if (!is_null($password)) {
            $this->password = $password;
        }
 
        $request = $this->getFreshRequest(false);
        $request->setAction('authenticate');
        $request->setData(array("secret" => $this->getPassword()));
        $request->httpMethod = "POST";
        $request->requestFormat = 'array';

        
        $response = $request->send(false);

        //$response = $this->getApiResponse('authenticate', array(), $data, 'POST', false);
        if ($request->httpCode == 403) {
            throw new Exception('Cannot authenticate Fresh REST API client -- wrong authentication key');
        }
        if (!isset($response['token'])) {
            throw new Exception('Cannot authenticate Fresh REST API client -- ' . $response['message']);
        }
        $token = $response['token'];

        // save the auth token to persistent storage
        $this->setToken($token);

        return $token;
    }


     /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set a password used for API authentication. Override this function to implement
     * SDK into your own component
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * Get an authentication token from a file. Load it from API if there is no file.
     * @return string key
     */
    public function getToken() {
        if ($this->token == null) {
            $this->token = $this->getData('token');

            // the file might not be there yet
            if (!$this->token) {
                $this->token = $this->authenticate();
            }
        }
        return $this->token;
    }
    
    public function setToken($token){
        $this->setData('token', $token, true);
    }

    /**
     * Set key value pair to the local configuration array
     * @param string $key
     * @param string $value
     * @param boolean $save save to the persistent storage
     */
    protected function setData($key, $value, $save = false) {
        if ($this->data == null) {
            $this->data = $this->loadData();
            if ($this->data == null) {
                $this->data = array();
            }
        }

        $this->data[$key] = $value;
        if ($save) {
            $this->saveData($this->data);
        }
    }

    /**
     * Get one key from persistem storage
     * @param string $key lookup key for the value
     * @return value or null if not found
     */
    protected function getData($key) {
        if ($this->data == null) {
            $this->data = $this->loadData();
            if ($this->data == null) {
                $this->data = array();
            }
        }
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }

    /**
     * Save SDK data to persistent storage. Override this function to use other storage than filesystem
     * @param type $data
     * @throws Exception
     */
    public function saveData($data) {
        $handle = fopen($this->dataPath, "w");
        $success = fwrite($handle, json_encode($data));
        if (!$success) {
            throw new Exception("Can't write to file " . $this->dataPath . " -- check file exists and is writable");
        }
    }

    /**
     * Load SDK data from persistent storage. Override this function to use other storage than filesystem
     * @return type
     */
    public function loadData() {
        if (file_exists($this->dataPath)) {
            $data = file_get_contents($this->dataPath, true);
            if ($data !== false) {
                return json_decode($data, true);
            }
        }
        return array();
    }



    /**
     * Set custom URL for the action. For example: users/id/orders.
     *
     * @param string $url relative url on the freshrealm server
     * @param array $apiOptions GET attributes (see FreshRealmRequest::$defaultApiOptions)
     * @param array $inputData POST/PUT data passed to the API call - array or json string
     * @param string $httpMethod POST/PUT/GET
     * @return array
     * @throws Exception API Error
     */
    public function customAction($url, $apiOptions = null, $inputData = null, $httpMethod = null, $latest = false) {
        $request = $this->getFreshRequest($apiOptions);
        $request->setAction($url);
        if ($inputData != null) {
            $request->setData($inputData);
        }
        if ($httpMethod != null) {
            $request->httpMethod = $httpMethod;
        }

        if ($latest) {

            $timestamp = $this->getData('timestamp_' . $url);
            
            if ($timestamp) {
                $apiOptions['timestamp'] = $timestamp;
            }
        }

        $data = $request->send();

        if ($request->httpCode == 200) {
            // save timestamp for index actions only
            if (!strpos($url, '/')){
                $this->setData('timestamp_' . $url, $data['timestamp'], true);
            }
            return $data;
        } else {
            throw new Exception($data['message']);
        }
    }
    
        /**
     * Download resources using pagination
     *
     * @param string $url relative url on the freshrealm server
     * @param array $apiOptions GET attributes (see FreshRealmRequest::$defaultApiOptions)
     * @param array $inputData POST/PUT data passed to the API call - array or json string
     * @param string $httpMethod POST/PUT/GET
     * @return array
     * @throws Exception API Error
     */
    public function paginationDownload($url, $latest = false, $apiOptions = ['limit' => 100]) {
        if ($latest) {
            $timestamp = $this->getData('timestamp_' . $url);
            if ($timestamp) {
                $apiOptions['timestamp'] = $timestamp;
            }
        }
        if (!isset($apiOptions['limit'])){
            $apiOptions['limit'] = 100;
        }
     
        $request = $this->getFreshRequest($apiOptions);
        $request->setAction($url);

        $returnedCount = 0;

        // send first request
        $data = $request->send();
        $output = $data;

        if ($request->httpCode == 200) {
            $returnedCount += count($data['data']);
            //\d("returned: $returnedCount, total $request->totalCount");
            //\d($request);
            while ($request->totalCount > $returnedCount) {
                $request->incrementPage();
                $data = $request->send();
                $returnedCount += count($data);
                $output = array_merge($output, $data);
            }
            // save timestamp for index actions only
            if (!strpos($url, '/')) {
                $this->setData('timestamp_' . $url, $data['timestamp'], true);
            }
            return $output;
        } else {
            throw new Exception($data['message']);
        }
    }

    /**
     * Get the latest timestamp returned from the server. The timestamp is in UTC time.
     * @return int
     */
    public function getLastCallTimestamp() {
        if ($this->lastRequest != null) {
            if (isset($this->lastRequest->timestamp)) {
                return $this->lastRequest->timestamp;
            }
        }
    }

    /**
     * Clear synchronization times from persistent storage. Call this function
     * to ensure that all the data is synchronized correctly after sync error
     * on the client side. Timestamps will be deleted and the next api call will
     * download all the resources.
     */
    public function clearTimestamps(){
        $data = $this->loadData();
        if (isset($data['token'])){
            $this->data = array('token' => $data->token);
        } else {
            $this->data = array();
        }
        $this->saveData();
    }
}
