<?php

class LitterboxSdk extends FreshSdk {
    
    private $_cat;
    
    public function setCat($cat){
        $this->_cat = $cat;
    }
    
    public function getToken(){
        return $this->_cat->token;
    }
    
    public function setToken($token){
         $this->_cat->token = $token;
    }
}