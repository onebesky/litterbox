<?php


/**
 * Model class for any resource passed to the FreshRealm Rest Api.
 * 
 * @author Ian McManus <ian@freshrealm.co>
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 * @copyright Copyright &copy; FreshRealm 2013
 * @package freshrealm.sdk
 * @version 1.0.0
 */
class FreshRealmResource {

    /**
     * Set data in the constructor.
     * @param array $data associative array (key=>value)
     */
    public function __construct($data = null) {
        if ($data != null) {
            $this->setData($data);
        }
    }

    /**
     * Update model from array
     * @param array $input
     */
    public function setData($input) {
        $myVars = get_object_vars($this);
        foreach ($input as $key => $value) {
            if (array_key_exists($key, $myVars)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Get class variables in array and include all nested resources
     * @return type
     */
    public function toArray() {
        $myVars = get_object_vars($this);

        foreach ($myVars as $key => $value) {
            if (is_a($value, 'FreshRealmResource')) {
                $myVars[$key] = $value->toArray();
            }
        }
        return $myVars;
    }

}