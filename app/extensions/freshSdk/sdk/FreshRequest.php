<?php

/**
 * Every call corresponds to one request.
 * <h2>Lifecycle</h2>
 * <ul>
 * <li>
 * Use $sdk->getFreshRealmRequest() to get
 * an instance of this class.
 * </li>
 * <li>Set all the attributes (setData, setApiOptions, setHttpMethod...)</li>
 * <li>Execute send() function to get response.</li>
 * <li>See $apiResponse, $requestData, and $requestUrl for debuggin information.</li>
 * </ul>
 *
 * @author Ian McManus <ian@freshrealm.co>
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 * @copyright Copyright &copy; FreshRealm 2013
 * @package freshrealm.sdk
 * @version 1.0.1
 */
class FreshRequest {

    /**
     * POST/PUT input data of the last call used for debuging.
     * @var array
     */
    public $requestData;

    /**
     * FreshRealm REST API output used for debuging. Stores reponse of the last call.
     * @var array
     */
    public $apiResponse;

    /**
     * Latest called url.
     * @var string
     */
    public $requestUrl;

    /**
     *
     * @var FreshRealmSdk
     */
    private $sdk;

    /**
     * GET (for list or view), POST (for create), PUT (for update)
     * @var string
     */
    public $httpMethod = "GET";

    /**
     * POST/PUT data format - json or array
     * @var string
     */
    public $requestFormat = 'json';

    /**
     * API operation
     * @var type
     */
    public $action;

    /**
     * GET input
     * @var array
     */
    public $getParams = array();

    /**
     * Generic API functionality - see $defaultApiOptions
     * @var array
     */
    public $apiOptions = null;

    /**
     * Use this array as a template for setting special filtering attributes
     * @var array
     */
    private static $defaultApiOptions = array(
        'limit' => 100,
        'offset' => 0,
        'zip' => null,
        'filter' => array(),
        'fields' => array(),
        'timestamp' => null
    );

    public $totalCount = 0;
    public $pageCount = 0;
    public $currentPage = 1;
    public $timestamp;

    public $httpCode;
    
    /**
     * We need to know the SDK instance in order to call authenticate function
     * and set the last response in the SDK
     *
     * @param FreshRealmSdk $sdk
     */
    public function __construct($sdk) {
        $this->sdk = $sdk;
    }

    /**
     * GET/POST/PUT
     * @param string $httpMethod
     */
    public function setHttpMethod($httpMethod) {
        $this->httpMethod = $httpMethod;
    }

    /**
     * Set the relative path of the Rest Url. For example: orders or customers/id/orders
     * @param string $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

    /**
     * Add additional GET attributes to the Rest Url.
     * @param array $params use key => value convention
     */
    public function addParams($params) {
        foreach ($params as $param => $value) {
            $this->getParams[$param] = $value;
        }
    }
    
    /**
     * Move page up for multi page download
     */
    public function incrementPage(){
        if ($this->apiOptions['limit'] > 0){
            $this->apiOptions['offset'] += $this->apiOptions['limit'];
        }
    }

    /**
     * Set input POST/PUT data
     * @param FreshRealmResource|array $inputData
     */
    public function setData($inputData) {
        if (is_a($inputData, 'FreshRealmResource')) {
            $this->requestData = $inputData->toArray();
        } else if (is_array($inputData)) {
            $this->requestData = $inputData;
        }
    }

    /**
     * This function uses CURL to send the response to the FreshRealm API server and returns the json response
     * It also should be configured to handle errors appropriately
     *
     * @param string $action
     * @param array $params array of filters to be passed into the url as key value pairs
     * @param array $inputData json formatted input data
     * @param string $dataMethod
     * @param boolean $checkCode
     * @return array json formatted array
     */
    public function send($reauthenticate = true) {

        // add authentication token to each request
        if ($this->action != 'authenticate' && !isset($this->getParams['key'])) {
            $token = $this->sdk->getToken();
            $this->getParams['key'] = $token;
        }

        $this->requestUrl = $this->getCompleteRequestUrl();

        $request = curl_init($this->requestUrl);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true
        );

        // set POST or PUT data
        if ($this->requestData != null) {
            if ($this->httpMethod == "POST") {
                $options[CURLOPT_POST] = 1;
            } else if ($this->httpMethod == "PUT") {
                $options[CURLOPT_CUSTOMREQUEST] = "PUT";
            }

            // prepare post data
            if ($this->requestFormat == 'json' && is_array($this->requestData)) {
                $this->requestData = json_encode($this->requestData);
            }

            if (is_array($this->requestData)) {
                $_fieldsString = "";
                foreach ($this->requestData as $key => $value) {
                    $_fieldsString .= $key . '=' . $value . '&';
                }
                $fieldsString = trim($_fieldsString, '&');
            } else {
                $options[CURLOPT_HTTPHEADER] = array('Content-Type: application/json', 'Content-Length: ' . strlen($this->requestData));
                $fieldsString = $this->requestData;
            }
            $options[CURLOPT_POSTFIELDS] = $fieldsString;
        }

        // \d("Sending FreshRealm API request", $this->requestUrl, $this->requestData);

        curl_setopt_array($request, $options);
        $result = curl_exec($request);
        //echo $result;
        $headers = $this->extractHeaders($result);
        if (isset($headers['X-Pagination-Total-Count'])){
            $this->totalCount = $headers['X-Pagination-Total-Count'];
        }
        if (isset($headers['X-Pagination-Total-Count'])){
            $this->pageCount = $headers['X-Pagination-Page-Count'];
        }
        if (isset($headers['X-Pagination-Current-Page'])){
            $this->currentPage = $headers['X-Pagination-Current-Page'];
        }
        //print_r($headers);
        $response = $this->extractResponse($result);
        //print_r($response);
        
        //handle errors
        if ($reauthenticate == true) {
            //if un-authenticated re-authenticate and try again
            if ($this->httpCode == 403 || $this->httpCode == 401) {

                //get new key and try again
                $key = $this->sdk->authenticate();
                $this->addParams(array('key' => $key));

                //try again make sure we return
                $newResponse = $this->send(false);

                return $newResponse;
            }
            if ($this->httpCode != 200) {
                //email site administrator and potentially freshrealm tech support
            }
        }
        $this->apiResponse = $response;

        // let SDK know what was executed (debugging purposes)
        $this->sdk->lastRequest = $this;

        return $response;
    }

    /**
     * Separate headers from body
     * @param type $response
     */
    private function extractHeaders($response) {
        $headers = array();

        $headerText = substr($response, 0, strrpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $headerText) as $i => $line) {
            if ($i === 0){
                $headers['http_code'] = $line;
            $this->httpCode = $line;
            } else {
                if(strstr($line, ':')) {
                    list ($key, $value) = explode(': ', $line);
                    $headers[$key] = $value;
                }
            }
        }
    if (isset($headers['X-Timestamp'])){
        $this->timestamp = $headers['X-Timestamp'];
    }
        return $headers;
    }

    private function extractResponse($response){
        $bodyText = substr($response, strrpos($response, "\r\n\r\n"));
        return json_decode($bodyText, true);
    }
    
    /**
     * Get request URL that includes apiOptions and custom get attributes
     * @return string
     */
    private function getCompleteRequestUrl() {
        // create url query string
        $_getString = "";

        // generic api params
        if ($this->apiOptions !== null) {
            if ($this->apiOptions['limit'] > 0) {
                $this->getParams['limit'] = $this->apiOptions['limit'];
            }
            if ($this->apiOptions['offset'] > 0) {
                $this->getParams['offset'] = $this->apiOptions['offset'];
            }
            if ($this->apiOptions['timestamp'] > 0) {
                $this->getParams['timestamp'] = $this->apiOptions['timestamp'];
            }
            if (count($this->apiOptions['filter']) > 0) {
                $this->getParams['filter'] = json_encode($this->apiOptions['filter']);
            }
            if (count($this->apiOptions['fields']) > 0) {
                $this->getParams['fields'] = json_encode($this->apiOptions['fields']);
            }
            if (strlen($this->apiOptions['zip']) > 0) {
                $this->getParams['zip'] = $this->apiOptions['zip'];
            }
        }

        // params passed from user
        foreach ($this->getParams as $key => $value) {
            $_getString .= $key . '=' . $value . '&';
        }

        $getString = trim($_getString, '&');

        $url = FreshSdk::$url . $this->action . (strlen($getString) ? "?" . $getString : "");

        $this->requestDataUrl = $url;
        return $url;
    }

    /**
     * Return array that can be passed to any API call to filter, search or limit
     */
    public function getApiOptions() {
        if ($this->apiOptions == null) {
            return self::$defaultApiOptions;
        } else {
            return $this->apiOptions;
        }
    }

    /**
     * Set generic api options for any call. See self::$defaultOptions.
     * @param array $options
     */
    public function setApiOptions($options) {
        $myOptions = $this->getApiOptions();
        foreach ($options as $option => $value) {
            $myOptions[$option] = $value;
        }
        $this->apiOptions = $myOptions;
    }

}