<?php

/**
 * Yii component wrapper for FreshRealm SDK
 * TODO: create most common functions from SDK here
 *
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 */
class YiiSdk extends CApplicationComponent {

    /**
     * Global system wide sdk
     * @var FreshSdk
     */
    private $sdk;

    /**
     * Cat authenticated SDK
     * @var type 
     */
    private $catSdk;
    /**
     *
     * @var type
     */
    public $password;
    public $url;

    public function init() {
        // TODO: use yii import
        include_once "FreshSdk.php";
        include_once "LitterboxSdk.php";

        parent::init();

        // create singleton
        
        if ($this->url != null) {
            FreshSdk::$url = $this->url;
        }
    }

    public function getCatSdk($cat){
        if ($this->sdk == null) {
            $this->sdk = new LitterboxSdk();
            $this->sdk->setPassword($cat->api_secret);
        }

    }
    
    public function getClientSdk() {
        if ($this->sdk == null) {
            $this->sdk = new FreshSdk();
            $this->sdk->setPassword($this->password);
        }

        return $this->sdk;
    }

}
