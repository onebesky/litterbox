<?php

/**
 * Litter box is using sqlite for easier install and needs extra migration
 */
class m140924_031716_fr_auth_table extends CDbMigration {

    public function up() {
        $this->createTable('fr_api_device', array(
            'id' => 'INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT',
            'token' => 'TEXT',
            'ip_address' => 'TEXT',
            'update_time' => 'TEXT',
            'connected_type' => 'TEXT',
            'connected_id' => 'TEXT'
        ));
    }

    public function down() {
        $this->dropTable('fr_api_device');
        return true;
    }

}
