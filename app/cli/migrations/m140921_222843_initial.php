<?php

class m140921_222843_initial extends CDbMigration {

    public function up() {
        $this->createTable('cat', [
            'id' => 'INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT',
            'name' => 'TEXT NOT NULL',
            'api_secret' => 'TEXT NOT NULL',
            'token' => 'TEXT',
            'create_time' => 'INTEGER',
            'update_time' => 'INTEGER',
        ]);

        $this->createTable('location', [
            'id' => 'INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT',
            'location_name' => 'TEXT NOT NULL',
            'create_time' => 'INTEGER',
            'update_time' => 'INTEGER',
        ]);

        $this->createTable('activity', [
            'id' => 'INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT',
            'cat_id' => 'INTEGER NOT NULL',
            'location_id' => 'INTEGER NOT NULL',
            'activity' => 'TEXT NOT NULL',
            'activity_time' => 'INTEGER',
            'create_time' => 'INTEGER',
            'update_time' => 'INTEGER',
        ]);

        $this->insert('cat', [
            'name' => 'Stinky',
            'api_secret' => 'vdbge7644tddu33gse3g15',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('cat', [
            'name' => 'Grumpy',
            'api_secret' => 'uurhhgjdiuh47837636fhg',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('cat', [
            'name' => 'Mr. Pickles',
            'api_secret' => 'oqqhgvbdgfjhr763dhhfd',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('location', [
            'location_name' => 'Kitchen',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('location', [
            'location_name' => 'Living Room',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('location', [
            'location_name' => 'Bedroom',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('location', [
            'location_name' => 'Bathroom',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('location', [
            'location_name' => 'Backyard',
            'create_time' => time(),
            'update_time' => time()
        ]);

        $this->insert('location', [
            'location_name' => 'Litterbox',
            'create_time' => time(),
            'update_time' => time()
        ]);
    }

    public function down() {
        echo "m140921_222843_initial does not support migration down.\n";
        return false;
    }

}
